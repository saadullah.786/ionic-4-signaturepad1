import { Component, ViewChild} from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform, ToastController, IonContent } from '@ionic/angular';
import { File, IWriteOptions } from '@ionic-native/file/ngx';
const STORAGE_KEY = 'IMAGE_LIST';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild('imageCanvas', { static: false }) canvas: any;
  @ViewChild(IonContent) content: IonContent;
  @ViewChild('fixedContainer') fixedContainer: any;

  canvasElement: any;
  saveX: number;
  saveY: number;
  lineWidth = 5;
  drawing = false;
  show_image: any;
  storedImages = [];
  selectedColor = '#9e2956';
 
  colors = [ '#9e2956', '#c2281d', '#de722f', '#edbf4c', '#5db37e', '#459cde', '#4250ad', '#802fa3' ];

  constructor(private plt: Platform, 
              private toastCtrl: ToastController,
              private storage:Storage,
              private file:File) { 
                this.storage.ready().then(() => {
                  this.storage.get(STORAGE_KEY).then(data => {
                    if (data != undefined) {
                      this.storedImages = data;
                    }
                  });
                });
              }

  ngAfterViewInit() {
    this.canvasElement = this.canvas.nativeElement;
    this.canvasElement.width = this.plt.width() + '';
    this.canvasElement.height = 200;

  }


  startDrawing(ev) {

    this.drawing = true;
    const canvasPosition = this.canvasElement.getBoundingClientRect();
    console.log(canvasPosition);
    this.saveX = ev.touches[0].pageX - canvasPosition.x;
    this.saveY = ev.touches[0].pageY - canvasPosition.y;
  }
  endDrawing() {
    console.log("end");
    this.drawing = false;

  }
  moved(ev) {

    if (!this.drawing) return;
    // console.log("move", ev);
    const canvasPosition = this.canvasElement.getBoundingClientRect();
    let ctx = this.canvasElement.getContext('2d');

    let currentX = ev.touches[0].pageX - canvasPosition.x;
    let currentY = ev.touches[0].pageY - canvasPosition.y;

    ctx.lineJoin = 'round';
    ctx.strokeStyle = this.selectedColor;
    ctx.lineWidth = this.lineWidth;

    ctx.beginPath();
    ctx.moveTo(this.saveX, this.saveY);
    ctx.lineTo(currentX, currentY);
    ctx.closePath();
    ctx.stroke();

    this.saveX = currentX;
    this.saveY = currentY;
  }

  clearCanvas() {
    let ctx = this.canvasElement.getContext('2d');
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  }

  saveSignature() {
    let dataUrl = this.canvasElement.toDataURL();
    // alert("data_url => " + dataUrl);
    this.clearCanvas();

    let name = new Date().getTime() + '.png';
  let path = this.file.dataDirectory;
  let options: IWriteOptions = { replace: true };

  var data = dataUrl.split(',')[1];
  let blob = this.b64toBlob(data, 'image/png');

 
  this.file.writeFile(path, name, blob, options).then(res => {
    this.storeImage(name);
  
  }, err => {
    console.log('error: '+ err);
  });
    
  }

selectColor(color) {
  this.selectedColor = color;
}

b64toBlob(b64Data, contentType) {
  contentType = contentType || '';
  var sliceSize = 512;
  var byteCharacters = atob(b64Data);
  var byteArrays = [];
 
  for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
    var slice = byteCharacters.slice(offset, offset + sliceSize);
 
    var byteNumbers = new Array(slice.length);
    for (var i = 0; i < slice.length; i++) {
      byteNumbers[i] = slice.charCodeAt(i);
    }
 
    var byteArray = new Uint8Array(byteNumbers);
 
    byteArrays.push(byteArray);
  }
 
  var blob = new Blob(byteArrays, { type: contentType });
  return blob;
}

storeImage(imageName) {
  let saveObj = { img: imageName };
  this.storedImages.push(saveObj);
  this.storage.set(STORAGE_KEY, this.storedImages).then(() => {
    setTimeout(() =>  {
       this.content.scrollToBottom();
    }, 500);
  });
}

getImagePath(imageName) {
  let path = this.file.dataDirectory + imageName;
  // https://ionicframework.com/docs/wkwebview/#my-local-resources-do-not-load
  // path = normalizeURL(path);
  let win: any = window;
  path = win.Ionic.WebView.convertFileSrc(path);
  // alert(path);
  return path;
}

removeImageAtIndex(index) {
  let removed = this.storedImages.splice(index, 1);
  this.file.removeFile(this.file.dataDirectory, removed[0].img).then(res => {
  }, err => {
    console.log('remove err; ' ,err);
  });
  this.storage.set(STORAGE_KEY, this.storedImages);
}
   
}
